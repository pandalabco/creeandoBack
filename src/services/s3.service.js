const AWS = require('aws-sdk');

const { env } = process;
const s3 = new AWS.S3({
  region: 'us-east-2',
  accessKeyId: 'AKIAW5PZQ4WZB453KQ54', // env.S3_API_KEY,
  secretAccessKey: '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', // env.S3_API_SECRET,
});

exports.uploadFile = async (data) => {
  const bucketName = `${env.IMAGES_BUCKET_NAME}`;
  return new Promise((resolve, reject) => {
    if (!data) {
      // eslint-disable-next-line prefer-promise-reject-errors
      return reject({ code: 404, message: 'No hay imagenes' });
    }

    const folder = 'products/_id/';
    const path = folder + data.name;
    const params = {
      Body: data.data,
      Bucket: bucketName,
      ACL: 'public-read',
      Key: path,
    };

    // eslint-disable-next-line no-shadow
    s3.upload(params, function (err, data) {
      if (err) {
        // eslint-disable-next-line prefer-promise-reject-errors
        return reject({ err, err_stack: err.stack });
      }

      return resolve({ location: data.Location, key: data.key });
    });
  });
};

exports.deleteFile = async (key) => {
  const bucketName = `${env.IMAGES_BUCKET_NAME}-${env.STAGE}`;
  return new Promise((resolve, reject) => {
    const params = {
      Bucket: bucketName,
      Key: key,
    };

    s3.deleteObject(params, function (err, data) {
      if (err) {
        // eslint-disable-next-line prefer-promise-reject-errors
        return reject({ err, err_stack: err.stack });
      }

      return resolve(data);
    });
  });
};
