const { XMLHttpRequest } = require('xmlhttprequest');
const { normalize, stripPrefix } = require('xml2js').processors;
const xml2js = require('xml2js');
const logger = require('../config/logger');

const cooCotizarCiudades = async (enviosData) => {
  function ajax(url) {
    return new Promise((resolve, reject) => {
      function createCORSRequest(method) {
        let xhr = new XMLHttpRequest();
        if ('withCredentials' in xhr) {
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url, false);
        } else if (typeof XDomainRequest !== 'undefined') {
          // eslint-disable-next-line no-undef
          xhr = new XDomainRequest();
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url);
        } else {
          // eslint-disable-next-line no-console
          logger.info('CORS not supported');
          xhr = null;
        }
        return xhr;
      }
      const str = `
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="https://ws.coordinadora.com/ags/1.5/server.php">
      <soapenv:Header/>
        <soapenv:Body>
            <ser:Cotizador_cotizar>
              <p>
                  <!--You may enter the following 11 items in any order-->
                  <nit>901441256</nit>
                  <div>00</div>
                  <cuenta>3</cuenta>
                  <producto>0</producto>
                  <origen>${enviosData.origen}</origen>
                  <destino>${enviosData.destino}</destino>
                  <valoracion>${enviosData.valoracion}</valoracion>
                  <nivel_servicio>
                    <!--Zero or more repetitions:-->
                    <item>${enviosData.nivelServicio}</item>
                  </nivel_servicio>
                  <detalle>
                    <!--Zero or more repetitions:-->
                    <item>
                        <!--You may enter the following 6 items in any order-->
                        <ubl>${enviosData.ubl}</ubl>
                        <alto>${enviosData.alto}</alto>
                        <ancho>${enviosData.ancho}</ancho>
                        <largo>${enviosData.largo}</largo>
                        <peso>${enviosData.peso}</peso>
                        <unidades>${enviosData.unidades}</unidades>
                    </item>
                    
                  </detalle>
                  <apikey>e2f2db52-6fdb-11eb-9439-0242ac130002</apikey>
                  <clave>mW4sZ1oI3fB7hP5w</clave>
              </p>
            </ser:Cotizador_cotizar>
        </soapenv:Body>
      </soapenv:Envelope>`;
      const xhr = createCORSRequest('POST');
      if (!xhr) {
        logger.info('XHR issue');
        return;
      }
      xhr.onload = async function () {
        const results = await xhr.responseText;
        xml2js.parseString(results, { mergeAttrs: true, tagNameProcessors: [stripPrefix, normalize] }, (err, result) => {
          if (err) {
            throw err;
          }
          resolve(result);
        });
      };
      xhr.onerror = reject;
      xhr.setRequestHeader('Content-Type', 'text/xml');
      xhr.send(str);
    });
  }
  const url = 'https://ws.coordinadora.com/ags/1.5/server.php';
  const defineres = await ajax(url);
  return defineres;
};

const cooGenerarGuia = async (enviosData) => {
  function ajax(url) {
    return new Promise((resolve, reject) => {
      function createCORSRequest(method) {
        let xhr = new XMLHttpRequest();
        if ('withCredentials' in xhr) {
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url, false);
        } else if (typeof XDomainRequest !== 'undefined') {
          // eslint-disable-next-line no-undef
          xhr = new XDomainRequest();
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url);
        } else {
          // eslint-disable-next-line no-console
          logger.info('CORS not supported');
          xhr = null;
        }
        return xhr;
      }
      const str = `
      <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="https://guias.coordinadora.com/ws/guias/1.6/server.php" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
        <soapenv:Header/>
        <soapenv:Body>
            <ser:Guias_generarGuia soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
              <p xsi:type="ser:Agw_typeGenerarGuiaIn">
                <codigo_remision xsi:type="xsd:string"></codigo_remision>
                <fecha xsi:type="xsd:string"></fecha>
                <id_cliente xsi:type="xsd:int">35246</id_cliente>
                <id_remitente xsi:type="xsd:int"></id_remitente>
                <nombre_remitente xsi:type="xsd:string">${enviosData.remitenteNombre}</nombre_remitente>
                <direccion_remitente xsi:type="xsd:string">${enviosData.remitenteDireccion}</direccion_remitente>
                <telefono_remitente xsi:type="xsd:string">${enviosData.remitenteTelefono}</telefono_remitente>
                <ciudad_remitente xsi:type="xsd:string">${enviosData.remitenteCiudad}</ciudad_remitente>
                <nit_destinatario xsi:type="xsd:string">${enviosData.destinatarioNit}</nit_destinatario>
                <div_destinatario xsi:type="xsd:string">1</div_destinatario>
                <nombre_destinatario xsi:type="xsd:string">${enviosData.destinatarioNombre}</nombre_destinatario>
                <direccion_destinatario xsi:type="xsd:string">${enviosData.destinatarioDireccion}</direccion_destinatario>
                <ciudad_destinatario xsi:type="xsd:string">${enviosData.destinatarioCiudad}</ciudad_destinatario>
                <telefono_destinatario xsi:type="xsd:string">${enviosData.destinatarioTelefono}</telefono_destinatario>
                <valor_declarado xsi:type="xsd:float">${enviosData.valor}</valor_declarado>
                <codigo_cuenta xsi:type="xsd:int">2</codigo_cuenta>
                <codigo_producto xsi:type="xsd:int">0</codigo_producto>
                <nivel_servicio xsi:type="xsd:int">1</nivel_servicio>
                <linea xsi:type="xsd:string"></linea>
                <contenido xsi:type="xsd:string">${enviosData.contenido}</contenido>
                <referencia xsi:type="xsd:string"></referencia>
                <observaciones xsi:type="xsd:string">${enviosData.observaciones}</observaciones>
                <estado xsi:type="xsd:string">IMPRESO</estado>
                <detalle SOAP-ENC:arrayType="ns1:Agw_typeGuiaDetalle[1]" xsi:type="ns1:ArrayOfAgw_typeGuiaDetalle">
                  <item xsi:type="ns1:Agw_typeGuiaDetalle">
                    <ubl xsi:type="xsd:int">0</ubl>
                    <alto xsi:type="xsd:float">${enviosData.alto}</alto>
                    <ancho xsi:type="xsd:float">${enviosData.ancho}</ancho>
                    <largo xsi:type="xsd:float">${enviosData.largo}</largo>
                    <peso xsi:type="xsd:float">${enviosData.peso}</peso>
                    <unidades xsi:type="xsd:int">${enviosData.unidades}</unidades>
                    <referencia xsi:type="xsd:string"></referencia>
                    <nombre_empaque xsi:type="xsd:string"></nombre_empaque>
                  </item>
                </detalle>
                <cuenta_contable xsi:type="xsd:string"></cuenta_contable>
                <centro_costos xsi:type="xsd:string"></centro_costos>
                <margen_izquierdo xsi:type="xsd:float">0</margen_izquierdo>
                <margen_superior xsi:type="xsd:float">0</margen_superior>
                <id_rotulo xsi:type="xsd:int">58</id_rotulo>
                <usuario_vmi xsi:type="xsd:string"></usuario_vmi>
                <formato_impresion xsi:type="xsd:string"></formato_impresion>
                <atributo1_nombre xsi:type="xsd:string"></atributo1_nombre>
                <atributo1_valor xsi:type="xsd:string"></atributo1_valor>
                <notificaciones xsi:type="ns1:ArrayOfAgw_typeNotificaciones"/>
                <atributos_retorno xsi:type="ns1:Agw_typeAtributosRetorno"/>
                <nro_doc_radicados xsi:type="xsd:string"></nro_doc_radicados>
                <nro_sobre xsi:type="xsd:string"></nro_sobre>
                <usuario xsi:type="xsd:string">vupi.ws</usuario>
                <clave xsi:type="xsd:string">d0e44b2bf44f79669c62d40c8c7a5ab3190d523862611e6390609357f4620999</clave>
              </p>
            </ser:Guias_generarGuia>
        </soapenv:Body>
      </soapenv:Envelope>`;
      const xhr = createCORSRequest('POST');
      if (!xhr) {
        logger.info('XHR issue');
        return;
      }
      xhr.onload = async function () {
        const results = await xhr.responseText;
        xml2js.parseString(results, { mergeAttrs: true, tagNameProcessors: [stripPrefix, normalize] }, (err, result) => {
          if (err) {
            throw err;
          }
          resolve(result);
        });
      };
      xhr.onerror = reject;
      xhr.setRequestHeader('Content-Type', 'text/xml');
      xhr.send(str);
    });
  }
  const url = 'https://guias.coordinadora.com/ws/guias/1.6/server.php';
  const defineres = await ajax(url);
  return defineres;
};

const cooGenerarRotulo = async (enviosData) => {
  function ajax(url) {
    return new Promise((resolve, reject) => {
      function createCORSRequest(method) {
        let xhr = new XMLHttpRequest();
        if ('withCredentials' in xhr) {
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url, false);
        } else if (typeof XDomainRequest !== 'undefined') {
          // eslint-disable-next-line no-undef
          xhr = new XDomainRequest();
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url);
        } else {
          // eslint-disable-next-line no-console
          logger.info('CORS not supported');
          xhr = null;
        }
        return xhr;
      }
      const str = `
      <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="https://guias.coordinadora.com/ws/guias/1.6/server.php">
        <soapenv:Header/>
        <soapenv:Body>
            <ser:Guias_imprimirRotulos soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
              <p xsi:type="ser:Agw_imprimirRotulosIn">
                  <!--You may enter the following 4 items in any order-->
                  <id_rotulo xsi:type="xsd:string">58</id_rotulo>
                  <codigos_remisiones xsi:type="soapenc:Array" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
                    <!--You may enter ANY elements at this point-->
                    <item>${enviosData}</item>
                  </codigos_remisiones>
                  <usuario xsi:type="xsd:string">vupi.ws</usuario>
                  <clave xsi:type="xsd:string">d0e44b2bf44f79669c62d40c8c7a5ab3190d523862611e6390609357f4620999</clave>
              </p>
            </ser:Guias_imprimirRotulos>
        </soapenv:Body>
      </soapenv:Envelope>`;
      const xhr = createCORSRequest('POST');
      if (!xhr) {
        logger.info('XHR issue');
        return;
      }
      xhr.onload = async function () {
        const results = await xhr.responseText;
        xml2js.parseString(results, { mergeAttrs: true, tagNameProcessors: [stripPrefix, normalize] }, (err, result) => {
          if (err) {
            throw err;
          }
          resolve(result);
        });
      };
      xhr.onerror = reject;
      xhr.setRequestHeader('Content-Type', 'text/xml');
      xhr.send(str);
    });
  }
  const url = 'https://guias.coordinadora.com/ws/guias/1.6/server.php';
  const defineres = await ajax(url);
  return defineres;
};

const cooProgramarRecogida = async (enviosData) => {
  function ajax(url) {
    return new Promise((resolve, reject) => {
      function createCORSRequest(method) {
        let xhr = new XMLHttpRequest();
        if ('withCredentials' in xhr) {
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url, false);
        } else if (typeof XDomainRequest !== 'undefined') {
          // eslint-disable-next-line no-undef
          xhr = new XDomainRequest();
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          xhr.open(method, url);
        } else {
          // eslint-disable-next-line no-console
          logger.info('CORS not supported');
          xhr = null;
        }
        return xhr;
      }
      const str = `
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="https://ws.coordinadora.com/ags/1.5/server.php">
        <soapenv:Header/>
        <soapenv:Body>
            <ser:Recogidas_programar>
              <p>
                  <!--You may enter the following 36 items in any order-->
                  <modalidad>1</modalidad>
                  <fecha_recogida>${enviosData.fechaRecogida}</fecha_recogida>
                  <ciudad_origen>${enviosData.ciudadOrigen}</ciudad_origen>
                  <ciudad_destino>${enviosData.ciudadDestino}</ciudad_destino>
                  <nombre_destinatario></nombre_destinatario>
                  <nit_destinatario></nit_destinatario>
                  <direccion_destinatario></direccion_destinatario>
                  <telefono_destinatario></telefono_destinatario>
                  <nombre_empresa>${enviosData.empreaRemitente}</nombre_empresa>
                  <nombre_contacto>${enviosData.empresaContacto}</nombre_contacto>
                  <direccion>${enviosData.direccionRecogida}</direccion>
                  <telefono>${enviosData.telefono}</telefono>
                  <producto>4</producto>
                  <referencia></referencia>
                  <nivel_servicio>1</nivel_servicio>
                  <guia_inicial></guia_inicial>
                  <nit_cliente>901441256</nit_cliente>
                  <div_cliente>01</div_cliente>
                  <persona_autoriza>${enviosData.autoriza}</persona_autoriza>
                  <telefono_autoriza>1</telefono_autoriza>
                  <tipo_notificacion></tipo_notificacion>
                  <destino_notificacion></destino_notificacion>
                  <valor_declarado>${enviosData.valorDeclarado}</valor_declarado>
                  <unidades></unidades>
                  <observaciones></observaciones>
                  <estado></estado>
                  <centro_costos></centro_costos>
                  <cuenta_contable></cuenta_contable>
                  <datafono></datafono>
                  <agente></agente>
                  <contenido></contenido>
                  <equipo></equipo>
                  <sub_equipo></sub_equipo>
                  <nit_remite></nit_remite>
                  <apikey>e2f2db52-6fdb-11eb-9439-0242ac130002</apikey>
                  <clave>mW4sZ1oI3fB7hP5w</clave>
              </p>
            </ser:Recogidas_programar>
        </soapenv:Body>
      </soapenv:Envelope>`;
      const xhr = createCORSRequest('POST');
      if (!xhr) {
        logger.info('XHR issue');
        return;
      }
      xhr.onload = async function () {
        const results = await xhr.responseText;
        xml2js.parseString(results, { mergeAttrs: true, tagNameProcessors: [stripPrefix, normalize] }, (err, result) => {
          if (err) {
            throw err;
          }
          resolve(result);
        });
      };
      xhr.onerror = reject;
      xhr.setRequestHeader('Content-Type', 'text/xml');
      xhr.send(str);
    });
  }
  const url = 'https://ws.coordinadora.com/ags/1.5/server.php';
  const defineres = await ajax(url);
  return defineres;
};

module.exports = {
  cooCotizarCiudades,
  cooGenerarGuia,
  cooGenerarRotulo,
  cooProgramarRecogida,
};
