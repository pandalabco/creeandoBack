const request = require('request');
// const axios = require('axios');

exports.generateTokenMU = () =>
  new Promise((resolve, reject) => {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      client_id: process.env.CLIENT_ID_MU_DEV,
      client_secret: process.env.CLIENT_SECRET_MU_DEV,
    };
    const options = {
      method: 'POST',
      url: `${process.env.URL_MU_DEV}/oauth/token`,
      headers,
      form: { grant_type: 'client_credentials' },
    };

    request({ ...options }, (error, response, body) => {
      if (error) {
        // console.log(error)
        reject(error);
      } else {
        // console.log("-------------------------------")
        resolve(JSON.parse(body));
      }
    });
  });
exports.calculateMU = (token, dataFront) =>
  new Promise((resolve, reject) => {
    const headers = {
      'content-type': 'application/json',
      access_token: token,
    };

    const options = {
      method: 'POST',
      url: `${process.env.URL_MU_DEV}/calculate`,
      body: dataFront,
      headers,
      json: true,
    };
    request(options, (error, response, body) => {
      if (error) {
        reject(error);
      } else {
        resolve(body);
      }
    });
  });

exports.dispatchOrderMU = (token, order) =>
  new Promise((resolve, reject) => {
    // console.log('CREAR SERVICIO DE MU ', user, order, delivery);
    const headers = {
      'content-type': 'application/json',
      access_token: token,
    };
    const options = {
      method: 'POST',
      url: `${process.env.URL_MU_DEV}/delivery/create`,
      headers,
      body: order,
      json: true,
    };
    request({ ...options }, (error, response, body) => {
      if (error) {
        // console.log(error)
        reject(error);
      } else {
        // console.log( body)
        resolve(body);
      }
    });
  });

exports.seguimientoOrderMU = (token, order) =>
  new Promise((resolve, reject) => {
    // console.log('CREAR SERVICIO DE MU ', user, order, delivery);
    const headers = {
      'content-type': 'application/json',
      access_token: token,
    };
    const options = {
      method: 'POST',
      url: `${process.env.URL_MU_DEV}/task`,
      headers,
      body: order,
      json: true,
    };
    request({ ...options }, (error, response, body) => {
      if (error) {
        // console.log(error)
        reject(error);
      } else {
        // console.log( body)
        resolve(body);
      }
    });
});
