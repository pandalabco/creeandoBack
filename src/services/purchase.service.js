const { Payment, Order, Purchase, Product2 } = require('../models');

const createPurchase = async (wenjoy, consumer, body) => {
  // 1. Include Fawn.
  // 2. Create orders.
  let payment = await Payment.create({
    purchase_id: wenjoy.purchase_id,
    purchase_link: wenjoy.payment_url,
    purchase_description: '',
    purchase_total_value: 5000,
    purchase_state: 'PURCHASE_STARTED',
    payedBy: consumer.id,
  });
  let purchase = await Purchase.create({
    orderdBy: consumer.id,
    payments: [payment.id],
  });
  const asyncRes = await Promise.all(
    body.purchaseList.map(async (i) => {
      console.log(i)
      const order = await Order.create({
        orderdBy: consumer.id,
        brand: i.brand,
        product: [i.product],
        variantId: i.variantId,
        qty: i.qry,
        purchaseId: purchase.id,
        payments: [payment.id],
        address: consumer.address,
        city: consumer.city,
      });
      return order;
    })
  );
  const resultOrders = await asyncRes.map((a) => a._id);
  // 4. Create purchase.
  // 3. Create payment.

  // 5. Update orders and payment with pucharse id product stock.
  payment = await Payment.findById(payment.id);
  payment.purchaseId = purchase.id;
  payment.orders = resultOrders;
  const paymentResult = await payment.save();
  purchase = await Purchase.findById(purchase.id);
  purchase.orders = resultOrders;
  await purchase.save();
  return paymentResult.toObject();
};

const getPurchasesById = async (id) => {
  let products = await Purchase.find({ orderdBy: id })
    .populate('orderdBy', ['name', 'phone', 'email'])
    .populate('brand', ['name'])
    .populate('product')
    .populate('orders')
    .populate({
      path: 'orders',
      populate: {
        path: 'product',
        model: 'Product',
      },
    })
    .populate({
      path: 'orders',
      populate: {
        path: 'brand',
      },
    })
    .populate('payments');
  /* products = await products.map(async (product,index)=>{
    console.log('purchase order not null', product.orders[0] !== null, 'purchase order true',!!product.orders[0],'purchase order false', !product.orders[0]);
    if(product.orders[0] !== null && product.orders[0] && product.orders[0].product === null){
        // console.log('check the ther side', product.orders, product.orders[0].product === null);
      return product;
    } else {
      // console.log('check the ther side mmmmmmmmm', product.orders[0] === null)
      return product;
    }
  }); */
  return products;
};

module.exports = {
  createPurchase,
  getPurchasesById,
};
