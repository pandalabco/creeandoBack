const httpStatus = require('http-status');
const logger = require('../config/logger');
const { User, Brand, Product } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createProduct = async (userBody) => {
  const product = await Product.create(userBody);
  const brand = await Brand.findById(product.brand);
  brand.products.push(product.id);
  brand.save();
  return product;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryProducts = async () => {
  const products = await Product.find({ visible: true }).populate('brand').populate('category').populate('subs');
  return products;
};

const getArrayOfProducts = async (productsToFind) => {
  const products = await Product.find({ _id: { $in: productsToFind } })
    .populate('brand', ['brandCity', 'brandAdress', 'name'])
    .exec();
  return products;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getProductById = async (id) => {
  return Product.findById(id);
};

const getProductByBrand = async (id) => {
  const products = await Product.find({ brand: id });
  return products;
};
/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getProductById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  Object.assign(user, updateBody);
  logger.info(updateBody);
  await user.save();
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getProductById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};

module.exports = {
  createProduct,
  queryProducts,
  getProductById,
  getProductByBrand,
  getUserByEmail,
  updateUserById,
  deleteUserById,
  getArrayOfProducts,
};
