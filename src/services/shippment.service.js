const httpStatus = require('http-status');
const { User, Shippment } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createShippment = async (userBody) => {
  console.log({
    order: userBody._id,
    payments: userBody.payments[0],
    orderdBy: userBody.orderdBy._id,
    brand: userBody.brand._id,
    tracker: userBody.trackId,
    cost: userBody.costo,
    transport: userBody.transport,
    recogida: userBody.recogida,
    rotulo: userBody.rotulo,
  })
  const user = await Shippment.create({
    order: userBody._id,
    payments: userBody.payments[0],
    orderdBy: userBody.orderdBy._id,
    brand: userBody.brand._id,
    tracker: userBody.trackId,
    cost: userBody.costo,
    transport: userBody.transport,
    recogida: userBody.recogida,
    rotulo: userBody.rotulo,
  });
  return user;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (filter, options) => {
  const users = await User.paginate(filter, options);
  return users;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return User.findById(id);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.remove();
  return user;
};

const getShippmentByBrand = async (id) => {
  const products = await Shippment.find({ brand: id })
    .populate('orderdBy', ['name', 'phone', 'email'])
    .populate('brand', ['name'])
    .populate('order');
  return products;
};

module.exports = {
  createShippment,
  queryUsers,
  getUserById,
  getUserByEmail,
  updateUserById,
  deleteUserById,
  getShippmentByBrand,
};
