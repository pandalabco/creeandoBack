const express = require('express');
const subController = require('../../controllers/sub.controller');

const router = express.Router();

router.post('/newSub', subController.createUser);
router.get('/allSub', subController.getUsers);

module.exports = router;
