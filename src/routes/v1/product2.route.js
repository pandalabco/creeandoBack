const express = require('express');
const auth = require('../../middlewares/auth');
const productController = require('../../controllers/product2.controller');

const router = express.Router();

router.get('/allProducts', productController.getProducts);
router.get('/csvTemplete', /* auth('getUsers'), */ productController.getCSVTemplete);
router.post('/uploadCSVProducts', productController.newCSVProducts);
router.post('/uploadPicture', productController.uploadPicture);
router.post('/newProduct', auth('getUsers'), productController.createProduct);
router.get('/allProductsByBrand', productController.getProductByBrand);
router.get('/productsById', productController.getProductById);
router.post('/updateProductsById', auth('getUsers'), productController.updateProduct);
router.post('/updateProducts', productController.updateProducts);
router.get('/coordinadora', productController.coordinadora);

module.exports = router;
