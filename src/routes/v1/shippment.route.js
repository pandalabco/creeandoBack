const express = require('express');
const auth = require('../../middlewares/auth');
const shippmentController = require('../../controllers/shippment.controller');

const router = express.Router();

// Generar envios mongo.
// Coordinadora
router.post('/coo', shippmentController.calculateCooService);
router.post('/cooGuia', shippmentController.generarGuiaCooService);
router.post('/cooRotulo', shippmentController.generarRotuloCooService);
router.post('/cooRecoleccion', shippmentController.solicitarRecoleccionCooService);
router.post('/cotizar', auth('getUsers'), shippmentController.calculateShipment);
router.post('/solicitarEnvio', auth('getUsers'), shippmentController.despatcharShipment);
router.get('/envios', auth('getUsers'), shippmentController.getShippmentsByBrand);

module.exports = router;
