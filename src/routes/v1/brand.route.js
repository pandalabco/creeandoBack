const express = require('express');
const auth = require('../../middlewares/auth');
const brandController = require('../../controllers/brand.controller');

const router = express.Router();

router.get('/getAllBrands', brandController.getBrands);
router.get('/brand', brandController.getBrand);
router.post('/brand', auth('getUsers'), brandController.createBrand);
router.post('/updatebrand',/*  auth('getUsers'), */ brandController.updateUser);
router.post('/uploadimages-brand', auth('getUsers'), brandController.uploadBrandImage);

module.exports = router;
