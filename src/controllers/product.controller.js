const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { userService, productService, coordinadoraService } = require('../services');

const createProduct = catchAsync(async (req, res) => {
  const products = await Promise.all(
    req.body.products.map(async (theproduct) => {
      const product = await productService.createProduct(theproduct);
      return product;
    })
  );
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Product created', data: products });
});

const getProducts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await productService.queryProducts(filter, options);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All products', data: result });
});

const getProductById = catchAsync(async (req, res) => {
  const product = await productService.getProductById(req.query.ID);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All products', data: product });
});

const getProductByBrand = catchAsync(async (req, res) => {
  const product = await productService.getProductByBrand(req.query.ID);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All products', data: product });
});

const updateProduct = catchAsync(async (req, res) => {
  const product = await productService.updateUserById(req.body.id, req.body);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: product updated', data: product });
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

const coordinadora = catchAsync(async (req, res) => {
  coordinadoraService.cooCotizarCiudades();
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Product created', data: null });
});

module.exports = {
  createProduct,
  getProducts,
  getProductById,
  getProductByBrand,
  updateProduct,
  deleteUser,
  coordinadora,
};
