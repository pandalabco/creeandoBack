const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { orderService } = require('../services');

const getOrdersByBrand = catchAsync(async (req, res) => {
  const orders = await orderService.getOrdersByBrand(req.query.ID);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All products', data: orders });
});

const updateOrder = catchAsync(async (req, res) => {
  const order = await orderService.updateUserById(req.query.ID, req.body);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Order actualizdo.', data: order });
});

module.exports = {
  getOrdersByBrand,
  updateOrder,
};
