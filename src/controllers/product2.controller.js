const httpStatus = require('http-status');
const { Parser } = require('json2csv');
const mongoose = require('mongoose');
const csv = require('fast-csv');
// const csv=require('csvtojson')
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { userService, product2Service, coordinadoraService, s3Service } = require('../services');
const logger = require('../config/logger');

const createProduct = catchAsync(async (req, res) => {
  const products = await Promise.all(
    req.body.products.map(async (theproduct) => {
      const product = await product2Service.createProduct(theproduct);
      return product;
    })
  );
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Product created', data: products });
});

const getProducts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await product2Service.queryProducts(filter, options);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All products', data: result });
});

const getProductById = catchAsync(async (req, res) => {
  const product = await product2Service.getProductById(req.query.ID);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All products', data: product });
});

const getProductByBrand = catchAsync(async (req, res) => {
  const product = await product2Service.getProductByBrand(req.query.ID);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All products', data: product });
});

const updateProduct = catchAsync(async (req, res) => {
  const product = await product2Service.updateUserById(req.body.id, req.body);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: product updated', data: product });
});

const updateProducts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await product2Service.queryProducts(filter, options);
  const products = await Promise.all(
    result.map(async (theproduct) => {
      const product = await product2Service.updateUserById(theproduct.id, { discount: 0 });
      return product;
    })
  );
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Product created', data: products });
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

const coordinadora = catchAsync(async (req, res) => {
  coordinadoraService.cooCotizarCiudades();
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Product created', data: null });
});

const getCSVTemplete = catchAsync(async (req, res) => {
  const fields = [
    'name',
    'slug',
    'shortDesc',
    'price',
    'brand',
    'shipping',
    'category',
    'subs',
    'pictures0',
    'pictures1',
    'pictures2',
    'pictures3',
    'pictures4',
    'pictures5',
    'pictures6',
    'variant0color',
    'variant0size',
    'variant0stock',
    'variant0img',
    'variant0visible',
    'variant0sku',
    'variant1color',
    'variant1size',
    'variant1stock',
    'variant1img',
    'variant1visible',
    'variant1sku',
    'variant2color',
    'variant2size',
    'variant2stock',
    'variant2img',
    'variant2visible',
    'variant2sku',
    'variant3color',
    'variant3size',
    'variant3stock',
    'variant3img',
    'variant3visible',
    'variant3sku',
    'variant4color',
    'variant4size',
    'variant4stock',
    'variant4img',
    'variant4visible',
    'variant4sku',
    'variant5color',
    'variant5size',
    'variant5stock',
    'variant5img',
    'variant5visible',
    'variant5sku',
    'variant6color',
    'variant6size',
    'variant6stock',
    'variant6img',
    'variant6visible',
    'variant6sku',
    'variant7color',
    'variant7size',
    'variant7stock',
    'variant7img',
    'variant7visible',
    'variant7sku',
    'variant8color',
    'variant8size',
    'variant8stock',
    'variant8img',
    'variant8visible',
    'variant8sku',
    'variant9color',
    'variant9size',
    'variant9stock',
    'variant9img',
    'variant9visible',
    'variant9sku',
    'variant10color',
    'variant10size',
    'variant10stock',
    'variant10img',
    'variant10visible',
    'variant10sku',
    'variant11color',
    'variant11size',
    'variant11stock',
    'variant11img',
    'variant11visible',
    'variant11sku',
    'variant12color',
    'variant12size',
    'variant12stock',
    'variant12img',
    'variant12visible',
    'variant12sku',
    'variant13color',
    'variant13size',
    'variant13stock',
    'variant13img',
    'variant13visible',
    'variant13sku',
    'variant14color',
    'variant14size',
    'variant14stock',
    'variant14img',
    'variant14visible',
    'variant14sku',
  ];
  const json2csvParser = new Parser({ fields });
  const csvTemplete = json2csvParser.parse({ data: '' });
  res.header('Content-Type', 'text/csv');
  res.attachment('file.csv');

  res.status(httpStatus.OK).send(csvTemplete);
});

const newCSVProducts = catchAsync(async (req, res) => {
  if (!req.files) return res.status(400).send('No files were uploaded.');
  const authorFile = req.files.data;
  const products = [];
  const stream = csv
    .parse({ headers: true, ignoreEmpty: true })
    .on('error', (error) => {
      logger.error(error);
      console.log(error);
    })
    .on('data', (row) => {
      // Revisar si hay variantes
      const pictures = [];
      // eslint-disable-next-line array-callback-return
      [...Array(7)].map((v, i) => {
        if (row[`pictures${i}`] !== undefined && row[`pictures${i}`] !== '') {
          pictures.push(row[`pictures${i}`]);
        }
      });
      // Revisar si hay variantes
      const variants = [];
      // eslint-disable-next-line array-callback-return
      [...Array(20)].map((v, i) => {
        if (row[`variant${i}stock`] !== undefined && row[`variant${i}stock`] !== '') {
          variants.push({
            // eslint-disable-next-line radix
            price: parseInt(row.price),
            color: row[`variant${i}color`],
            size: row[`variant${i}size`],
            // eslint-disable-next-line radix
            stock: parseInt(row[`variant${i}stock`]),
            img: row[`variant${i}img`],
            visible: true,
            sku: row.variant0sku,
          });
        }
      });
      // Producto final.
      const product = {
        id: new mongoose.Types.ObjectId(),
        name: row.name,
        slug: row.slug,
        shortDesc: row.shortDesc,
        // eslint-disable-next-line radix
        price: parseInt(row.price),
        category: row.category,
        brand: row.brand,
        subs: [row.subs],
        variant: variants,
        sold: 0,
        pictures,
        smPictures: pictures,
        shipping: row.shipping,
        ratings: 0,
        reviews: 0,
      };
      products.push(product);
    })
    .on('end', async (rowCount) => {
      logger.info(`Parsed ${rowCount} rows`);
      const createdProducts = await Promise.all(
        products.map(async (theproduct) => {
          const product = await product2Service.createProduct(theproduct);
          return product;
        })
      );
      res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Products created', data: createdProducts });
    });
  stream.write(authorFile.data.toString().replace(/;/g, ','));
  stream.end();
});

const uploadPicture = catchAsync(async (req, res) => {
  if (!req.files) return res.status(400).send('No files were uploaded.');
  const authorFile = req.files.picture;
  // console.log(authorFile);
  s3Service
    .uploadFile(authorFile)
    .then(async (uploadedImage) => {
      console.log(uploadedImage);
      // const productUpdated = await service.updateProduct({ $push:{images: uploadedImage._id }}, _id)
      res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Image created', data: uploadedImage });
    })
    .catch((errorToCreate) => {
      // console.log(errorToCreate);
      res.status(httpStatus.OK).json({ code: httpStatus[400], message: 'Error: Image created', data: errorToCreate });
    });
});

module.exports = {
  createProduct,
  getProducts,
  getProductById,
  getProductByBrand,
  updateProduct,
  updateProducts,
  deleteUser,
  coordinadora,
  getCSVTemplete,
  newCSVProducts,
  uploadPicture,
};
