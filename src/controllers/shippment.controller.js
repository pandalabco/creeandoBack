const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { format } = require( 'date-fns');
const { coordinadoraService, muService, productService, product2Service, orderService, shippmentService } = require('../services');

const calculateShipment = catchAsync(async (req, res) => {
  // const envio = await coordinadoraService.cooCotizarCiudades(req.body);
  /* if (!req.user.name || !req.user.lastName || !req.user.idType || !req.user.idDoc || !req.user.address || !req.user.city) {
    res.status(httpStatus[400]).json({
      code: httpStatus[400],
      message: 'Error datos incompletos',
      data: {
        error:
          'Para cotizar o realizar una compra requieres: nombre, apellido, tipo de documento, numero de documento, direccion, ciudad',
        link: 'https://ciudadvirtual.co',
      },
    });
  } */
  const products = await product2Service.getArrayOfProducts(req.body.pricing);
  console.log(products);
  const coti = await Promise.all(
    products.map(async (product) => {
      if (product.shipping === 'No') {
        return { ...product.toObject(), transport: 'No tiene envío', costo: 0, trackId: 0 };
      }
      const array1 = ['Bogota', 'Cali', 'Medellin', 'Barranquilla'];
      const found = array1.find((element) => element === product.brand.brandCity);
      if (product.brand.brandCity === req.user.city && found !== undefined) {
        let finalUserCity;
        if (req.user.city === 'Bogota') {
          finalUserCity = 1;
        } else if (req.user.city === 'Cali') {
          finalUserCity = 2;
        } else if (req.user.city === 'Medellin') {
          finalUserCity = 3;
        } else if (req.user.city === 'Barranquilla') {
          finalUserCity = 4;
        }
        const token = await muService.generateTokenMU();
        const response = await muService.calculateMU(token.access_token, {
          id_user: 761838,
          type_service: 4,
          roundtrip: 0,
          declared_value: 1250,
          city: finalUserCity,
          parking_surcharge: 2000,
          coordinates: [
            {
              address: product.brand.brandAdress,
              city: product.brand.brandCity,
            },
            {
              address: req.user.address,
              city: req.user.city,
            },
          ],
        });
        console.log(response); 
        return { ...product.toObject(), transport: 'MU', costo: response.data.total_service };
      }
      let finalCOOUserCity;
      if (req.user.city === 'Bogota') {
        finalCOOUserCity = '11001000';
      } else if (req.user.city === 'Cali') {
        finalCOOUserCity = '76001000';
      } else if (req.user.city === 'Medellin') {
        finalCOOUserCity = '05001000';
      } else if (req.user.city === 'Barranquilla') {
        finalCOOUserCity = '08001000';
      } else if (req.user.city === 'Cartagena') {
        finalCOOUserCity = '13001000';
      } else if (req.user.city === 'Soledad') {
        finalCOOUserCity = '08758000';
      } else if (req.user.city === 'Ibague') {
        finalCOOUserCity = '73001000';
      } else if (req.user.city === 'Chia') {
        finalCOOUserCity = '25175000';
      } else if (req.user.city === 'Soacha') {
        finalCOOUserCity = '25754000';
      } else if (req.user.city === 'Villavicencio') {
        finalCOOUserCity = '50001000';
      } else if (req.user.city === 'Bucaramanga') {
        finalCOOUserCity = '68001000';
      } else if (req.user.city === 'Santa Marta') {
        finalCOOUserCity = '47001000';
      } else if (req.user.city === 'Valledupar') {
        finalCOOUserCity = '20001000';
      } else if (req.user.city === 'Bello') {
        finalCOOUserCity = '05088000';
      } else if (req.user.city === 'Itagui') {
        finalCOOUserCity = '05360000';
      } else if (req.user.city === 'Envigado') {
        finalCOOUserCity = '05266000';
      } else if (req.user.city === 'San Pedro de los Milagros') {
        finalCOOUserCity = '05664000';
      } else if (req.user.city === 'Apartado') {
        finalCOOUserCity = '05045000';
      } else if (req.user.city === 'Turbo') {
        finalCOOUserCity = '05837000';
      } else if (req.user.city === 'Pereira') {
        finalCOOUserCity = '66001000';
      } else if (req.user.city === 'Dosquebradas') {
        finalCOOUserCity = '66170000';
      }
      let finalCOOBrandCity;
      if (product.brand.brandCity === 'Bogota') {
        finalCOOBrandCity = '11001000';
      } else if (product.brand.brandCity === 'Cali') {
        finalCOOBrandCity = '76001000';
      } else if (product.brand.brandCity === 'Medellin') {
        finalCOOBrandCity = '05001000';
      } else if (product.brand.brandCity === 'Barranquilla') {
        finalCOOBrandCity = '08001000';
      } else if (product.brand.brandCity === 'Cartagena') {
        finalCOOBrandCity = '13001000';
      } else if (product.brand.brandCity === 'Soledad') {
        finalCOOBrandCity = '08758000';
      } else if (product.brand.brandCity === 'Ibague') {
        finalCOOBrandCity = '73001000';
      } else if (product.brand.brandCity === 'Chia') {
        finalCOOBrandCity = '25175000';
      } else if (product.brand.brandCity === 'Soacha') {
        finalCOOBrandCity = '25754000';
      } else if (product.brand.brandCity === 'Villavicencio') {
        finalCOOBrandCity = '50001000';
      } else if (product.brand.brandCity === 'Bucaramanga') {
        finalCOOBrandCity = '68001000';
      } else if (product.brand.brandCity === 'Santa Marta') {
        finalCOOBrandCity = '47001000';
      } else if (product.brand.brandCity === 'Valledupar') {
        finalCOOBrandCity = '20001000';
      } else if (product.brand.brandCity === 'Bello') {
        finalCOOBrandCity = '05088000';
      } else if (product.brand.brandCity === 'Itagui') {
        finalCOOBrandCity = '05360000';
      } else if (product.brand.brandCity === 'Envigado') {
        finalCOOBrandCity = '05266000';
      } else if (product.brand.brandCity === 'San Pedro de los Milagros') {
        finalCOOBrandCity = '05664000';
      } else if (product.brand.brandCity === 'Apartado') {
        finalCOOBrandCity = '05045000';
      } else if (product.brand.brandCity === 'Turbo') {
        finalCOOBrandCity = '05837000';
      } else if (product.brand.brandCity === 'Pereira') {
        finalCOOBrandCity = '66001000';
      } else if (product.brand.brandCity === 'Dosquebradas') {
        finalCOOBrandCity = '66170000';
      }
      const envio = await coordinadoraService.cooCotizarCiudades({
        origen: finalCOOUserCity,
        destino: finalCOOBrandCity,
        valoracion: '20000',
        nivelServicio: '1',
        ubl: '0',
        alto: '10',
        ancho: '10',
        largo: '10',
        peso: '1',
        unidades: '1',
      });
      return {
        ...product.toObject(),
        transport: 'COO',
        costo: parseInt(envio.envelope.body[0].cotizador_cotizarresponse[0].cotizador_cotizarresult[0].flete_total[0]),
      };
    })
  );
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Cotización de envíos', data: coti });
});

const despatcharShipment = catchAsync(async (req, res) => {
  const ordenes = await orderService.getArrayOfOrders(req.body.orders);
  const despachos = await Promise.all(
    ordenes.map(async (orden) => {
      const array1 = ['Bogota', 'Cali', 'Medellin', 'Barranquilla'];
      const found = array1.find((element) => element === orden.brand.brandCity);
      if (orden.brand.brandCity === orden.orderdBy.city && found !== undefined) {
        // DESPACHOS MENSAJEROS URBANOS
        let finalUserCity;
        if (orden.orderdBy.city === 'Bogota') {
          finalUserCity = 1;
        } else if (orden.orderdBy.city === 'Cali') {
          finalUserCity = 2;
        } else if (orden.orderdBy.city === 'Medellin') {
          finalUserCity = 3;
        } else if (orden.orderdBy.city === 'Barranquilla') {
          finalUserCity = 4;
        }
        const token = await muService.generateTokenMU();
        const response = await muService.dispatchOrderMU(token.access_token, {
          id_user: 761838,
          type_service: 4,
          roundtrip: 0,
          declared_value: 1250,
          city: finalUserCity,
          start_date: '2017-04-18',
          start_time: '15:59:00',
          observation: `Recolección en ${orden.brand.name}, cel: ${orden.brand.brandCel}, email: ${orden.brand.brandEmil}, direccion: ${orden.brand.brandAdress}, ${orden.brand.brandCity} y entrega a ${orden.orderdBy.name}, cel: ${orden.orderdBy.phone}, email: ${orden.orderdBy.email}, direccion: ${orden.orderdBy.address}, ${orden.orderdBy.city}`,
          user_payment_type: 3,
          type_segmentation: 1,
          type_task_cargo_id: 2,
          os: 'NEW API 2.0',
          coordinates: [
            {
              type: '0',
              id_point: '1',
              address: orden.brand.brandAdress,
              city: orden.brand.brandCity,
              order_id: orden.id,
              description: `Recolleción en ${orden.brand.name}, cel: ${orden.brand.brandCel}, email: ${orden.brand.brandEmil}, direccion: ${orden.brand.brandAdress}, ${orden.brand.brandCity}`,
            },
            {
              type: '1',
              order_id: orden.id,
              address: orden.orderdBy.address,
              token: 234,
              city: orden.orderdBy.city,
              description: `Entrega a ${orden.orderdBy.name}, cel: ${orden.orderdBy.phone}, email: ${orden.orderdBy.email}, direccion: ${orden.orderdBy.address}, ${orden.orderdBy.city}`,
              client_data: {
                client_name: orden.orderdBy.name,
                client_phone: orden.orderdBy.phone,
                client_email: orden.orderdBy.email,
                products_value: '1000',
                domicile_value: '0',
                client_document: orden.orderdBy.idDoc,
                payment_type: 3,
              },
              products: [
                {
                  store_id: '1',
                  sku: orden.product.id,
                  product_name: orden.product.name,
                  url_img: orden.product.pictures[0],
                  value: 92100,
                  quantity: 1,
                  barcode: orden.product.id,
                  planogram: orden.brand.name,
                },
              ],
            },
          ],
        });
        if (response.status_code !== 200) {
          return { ...orden.toObject(), transport: 'MU', costo: response.status_code, trackId: response.message };
        }
        return { ...orden.toObject(), transport: 'MU', costo: response.data.total, trackId: response.data.task_id };
      }
      // DESPACHOS COORDINADORA
      let finalCOOUserCity;
      if (req.user.city === 'Bogota') {
        finalCOOUserCity = '11001000';
      } else if (req.user.city === 'Cali') {
        finalCOOUserCity = '76001000';
      } else if (req.user.city === 'Medellin') {
        finalCOOUserCity = '05001000';
      } else if (req.user.city === 'Barranquilla') {
        finalCOOUserCity = '08001000';
      } else if (req.user.city === 'Cartagena') {
        finalCOOUserCity = '13001000';
      } else if (req.user.city === 'Soledad') {
        finalCOOUserCity = '08758000';
      } else if (req.user.city === 'Ibague') {
        finalCOOUserCity = '73001000';
      } else if (req.user.city === 'Chia') {
        finalCOOUserCity = '25175000';
      } else if (req.user.city === 'Soacha') {
        finalCOOUserCity = '25754000';
      } else if (req.user.city === 'Villavicencio') {
        finalCOOUserCity = '50001000';
      } else if (req.user.city === 'Bucaramanga') {
        finalCOOUserCity = '68001000';
      } else if (req.user.city === 'Santa Marta') {
        finalCOOUserCity = '47001000';
      } else if (req.user.city === 'Valledupar') {
        finalCOOUserCity = '20001000';
      } else if (req.user.city === 'Bello') {
        finalCOOUserCity = '05088000';
      } else if (req.user.city === 'Itagui') {
        finalCOOUserCity = '05360000';
      } else if (req.user.city === 'Envigado') {
        finalCOOUserCity = '05266000';
      } else if (req.user.city === 'San Pedro de los Milagros') {
        finalCOOUserCity = '05664000';
      } else if (req.user.city === 'Apartado') {
        finalCOOUserCity = '05045000';
      } else if (req.user.city === 'Turbo') {
        finalCOOUserCity = '05837000';
      } else if (req.user.city === 'Pereira') {
        finalCOOUserCity = '66001000';
      } else if (req.user.city === 'Dosquebradas') {
        finalCOOUserCity = '66170000';
      }
      let finalCOOBrandCity;
      if (orden.brand.brandCity === 'Bogota') {
        finalCOOBrandCity = '11001000';
      } else if (orden.brand.brandCity === 'Cali') {
        finalCOOBrandCity = '76001000';
      } else if (orden.brand.brandCity === 'Medellin') {
        finalCOOBrandCity = '05001000';
      } else if (orden.brand.brandCity === 'Barranquilla') {
        finalCOOBrandCity = '08001000';
      } else if (orden.brand.brandCity === 'Cartagena') {
        finalCOOBrandCity = '13001000';
      } else if (orden.brand.brandCity === 'Soledad') {
        finalCOOBrandCity = '08758000';
      } else if (orden.brand.brandCity === 'Ibague') {
        finalCOOBrandCity = '73001000';
      } else if (orden.brand.brandCity === 'Chia') {
        finalCOOBrandCity = '25175000';
      } else if (orden.brand.brandCity === 'Soacha') {
        finalCOOBrandCity = '25754000';
      } else if (orden.brand.brandCity === 'Villavicencio') {
        finalCOOBrandCity = '50001000';
      } else if (orden.brand.brandCity === 'Bucaramanga') {
        finalCOOBrandCity = '68001000';
      } else if (orden.brand.brandCity === 'Santa Marta') {
        finalCOOBrandCity = '47001000';
      } else if (orden.brand.brandCity === 'Valledupar') {
        finalCOOBrandCity = '20001000';
      } else if (orden.brand.brandCity === 'Bello') {
        finalCOOBrandCity = '05088000';
      } else if (orden.brand.brandCity === 'Itagui') {
        finalCOOBrandCity = '05360000';
      } else if (orden.brand.brandCity === 'Envigado') {
        finalCOOBrandCity = '05266000';
      } else if (orden.brand.brandCity === 'San Pedro de los Milagros') {
        finalCOOBrandCity = '05664000';
      } else if (orden.brand.brandCity === 'Apartado') {
        finalCOOBrandCity = '05045000';
      } else if (orden.brand.brandCity === 'Turbo') {
        finalCOOBrandCity = '05837000';
      } else if (orden.brand.brandCity === 'Pereira') {
        finalCOOBrandCity = '66001000';
      } else if (orden.brand.brandCity === 'Dosquebradas') {
        finalCOOBrandCity = '66170000';
      }

      const envio = await coordinadoraService.cooGenerarGuia({
        remitenteNombre: orden.brand.name,
        remitenteDireccion: orden.brand.brandAdress,
        remitenteTelefono: orden.brand.brandCel,
        remitenteCiudad: finalCOOBrandCity,
        destinatarioNit: orden.orderdBy.idDoc,
        destinatarioNombre: orden.orderdBy.name,
        destinatarioDireccion: orden.orderdBy.address,
        destinatarioTelefono: orden.orderdBy.phone,
        destinatarioCiudad: finalCOOUserCity,
        valor: 20000,
        contenido: orden.product.name,
        referencia: orden.product.id,
        observaciones: `${orden.orderdBy.name}`,
        alto: 10,
        ancho: 10,
        largo: 10,
        peso: 1,
        unidades: 1,
        rederencia: orden.product.id,
      });
      const rotulo = await coordinadoraService.cooGenerarRotulo(envio.envelope.body[0].guias_generarguiaresponse[0].return[0].codigo_remision[0]._);
      const recoleccion = await coordinadoraService.cooProgramarRecogida({
        fechaRecogida: format(Date.now(), 'yyyy/MM/dd'),
        ciudadOrigen: finalCOOBrandCity,
        ciudadDestino: finalCOOUserCity,
        empreaRemitente: orden.brand.name,
        empresaContacto: orden.brand.name,
        direccionRecogida: orden.brand.brandAdress,
        telefono: orden.brand.brandCel,
        producto: orden.product.name,
        autoriza: 'cree-ando',
        valorDeclarado: 40000,
      });
      if (recoleccion.envelope.body[0].fault) {
        return {
          ...orden.toObject(),
          transport: 'COO',
          costo: 0,
          trackId: envio.envelope.body[0].guias_generarguiaresponse[0].return[0].codigo_remision[0]._,
          recogida: recoleccion.envelope.body[0].fault[0],
          rotulo: rotulo.envelope.body[0].guias_imprimirrotulosresponse[0].return[0].rotulos[0]._,
        };
      }
      return {
        ...orden.toObject(),
        transport: 'COO',
        costo: 0,
        trackId: envio.envelope.body[0].guias_generarguiaresponse[0].return[0].codigo_remision[0]._,
        recogida: recoleccion.envelope.body[0].recogidas_programarresponse[0].recogidas_programarresult[0],
        rotulo: rotulo.envelope.body[0].guias_imprimirrotulosresponse[0].return[0].rotulos[0]._,
      };
    })
  );
  const newshippment = await shippmentService.createShippment(despachos[0]);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Despachar orden', data: { despachos, newshippment} });
});

const calculateCooService = catchAsync(async (req, res) => {
  const envio = await coordinadoraService.cooCotizarCiudades(req.body);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Cotización con coordinadora', data: envio });
});

const calculateMUService = catchAsync(async (req, res) => {
  const token = await muService.generateTokenMU();
  const response = await muService.calculateMU(token.access_token, req.body);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Calcular servicio Mu', data: response });
});

const disptchMuService = catchAsync(async (req, res) => {
  const token = await muService.generateTokenMU();
  const response = await muService.dispatchOrderMU(token.access_token, req.body.user, req.body.order, req.body.delivery);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Calcular servicio Mu', data: response });
});

const getShippmentsByBrand = catchAsync(async (req, res) => {
  const envios = await shippmentService.getShippmentByBrand(req.query.ID);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Todos mis envios', data: envios });
});

const generarGuiaCooService = catchAsync(async (req, res) => {
  const envio = await coordinadoraService.cooGenerarGuia({
    remitenteNombre: 'Panda',
    remitenteDireccion: 'Cra 8a #108a -66',
    remitenteTelefono: '3123736141',
    remitenteCiudad: '11001000',
    destinatarioNit: '1020814134',
    destinatarioNombre: 'Matty',
    destinatarioDireccion: 'calle 145 #19-37',
    destinatarioTelefono: '3123736141',
    destinatarioCiudad: '11001000',
    valor: 20000,
    contenido: 'Cascoproff',
    referencia: '31234',
    observaciones: `Solo entregar y ya.`,
    alto: 10,
    ancho: 10,
    largo: 10,
    peso: 1,
    unidades: 1,
    rederencia: '31234',
  });
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Guía coordinadora', data: envio });
});

const generarRotuloCooService = catchAsync(async (req, res) => {
  const envio = await coordinadoraService.cooGenerarRotulo();
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Rótulo coordinadora', data: envio });
});

const solicitarRecoleccionCooService = catchAsync(async (req, res) => {
  const envio = await coordinadoraService.cooProgramarRecogida({
    fechaRecogida:'2021/5/25',
    ciudadOrigen: '11001000',
    ciudadDestino: '11001000',
    empreaRemitente: 'cree-ando',
    empresaContacto: 'cree-ando',
    direccionRecogida: 'cra 8a #108a -66',
    telefono: '3123736141',
    producto: 'Camisa cree-ando',
    autoriza: 'cree-ando',
    valorDeclarado: 40000,
  });
  res
    .status(httpStatus.OK)
    .json({ code: httpStatus.OK, message: 'Success: Solicitar recolección coordinadora', data: envio });
});
/* 
SEGUIMIENTO MU
{
    "id_user":xxxx,
    "task_id":xxxx
}
 */

module.exports = {
  calculateShipment,
  despatcharShipment,
  calculateCooService,
  calculateMUService,
  disptchMuService,
  getShippmentsByBrand,
  generarGuiaCooService,
  generarRotuloCooService,
  solicitarRecoleccionCooService,
};
