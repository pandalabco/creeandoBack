const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { brandService, cloudinaryService, userService, emailService } = require('../services');

const createBrand = catchAsync(async (req, res) => {
  const newBrand = { ...req.body, admins: [req.user.id] };
  const brand = await brandService.createBrand(newBrand);
  await userService.updateUserById(req.user.id, { admin: [...req.user.admin, brand.id] });
  const to = req.user.email;
  const subject = 'Gracias por crear una cuenta con nosotros';
  const html = `
  <img src=${brand.logo} style="height: 100px; width: 100vw; object-fit: cover; border-radius: 25px 25px 0px 0px;">
  <h1>¡Has creado una Marca!</h1>
  <p>
    Gracias ${req.user.name} por crear tu marca en el marketplace de Cree-Ando.
  </p>
  <img src="https://cree-ando.com/wp-content/uploads/2021/04/Mundo-virtual-Cree-Ando-foro-2-1024x570.png" style="height: 100px; width: 100vw; object-fit: cover; border-radius: 0px 0px 25px 25px;">
  `;
  await emailService.sendEmail(to, subject, html);
  res.status(httpStatus.CREATED).json({ code: httpStatus.CREATED, message: 'Success: Brand created', data: brand });
});

const uploadBrandImage = catchAsync(async (req, res) => {
  const brand = await cloudinaryService.upload(req.body);
  res.status(httpStatus.CREATED).json({ code: httpStatus.CREATED, message: 'Success: Brand image uploaded', data: brand });
});

const getBrands = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await brandService.queryBrands(filter, options);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All brands', data: result });
});

const getBrand = catchAsync(async (req, res) => {
  const brand = await brandService.getBrandById(req.query.ID);
  if (!brand) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Brand not found');
  }
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All brands', data: brand });
});

const updateUser = catchAsync(async (req, res) => {
  // console.log(req.body, req.files);
  const user = await brandService.updateUserById(req.body.id, req.body);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: Updated brand', data: user });
});

const deleteUser = catchAsync(async (req, res) => {
  await brandService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createBrand,
  getBrands,
  getBrand,
  updateUser,
  deleteUser,
  uploadBrandImage,
};
