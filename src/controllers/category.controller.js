const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { categoryService } = require('../services');

const createUser = catchAsync(async (req, res) => {
  const user = await categoryService.createUser(req.body);
  res.status(httpStatus.CREATED).json(user);
});

const getCats = catchAsync(async (req, res) => {
  const result = await categoryService.getCategorys();
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'me', data: result });
});

const getUser = catchAsync(async (req, res) => {
  const user = await categoryService.getUserById(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

const updateUser = catchAsync(async (req, res) => {
  const user = await categoryService.updateUserById(req.params.userId, req.body);
  res.send(user);
});

const deleteUser = catchAsync(async (req, res) => {
  await categoryService.deleteUserById(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createUser,
  getCats,
  getUser,
  updateUser,
  deleteUser,
};
