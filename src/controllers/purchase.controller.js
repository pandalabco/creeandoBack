const httpStatus = require('http-status');
const Joi = require('joi');
const catchAsync = require('../utils/catchAsync');
const { purchaseService, wenjoyService, emailService } = require('../services');
const logger = require('../config/logger');

const createPurchase = catchAsync(async (req, res) => {
  logger.info('create purchase', req.user);
  const requiredSchema = Joi.object().keys({
    name: Joi.string().min(3).required(),
    lastName: Joi.string().min(3).required(),
    email: Joi.string().min(3).required(),
    idDoc: Joi.string().min(3).required(),
    idType: Joi.number().required(),
    phone: Joi.number().min(3).required(),
    city: Joi.string().min(3).required(),
    address: Joi.string().min(3).required(),
  }).unknown(true);
  
  const requiredBodySchema = Joi.object().keys({
    purchaseList: Joi.array().min(1).required(),
    descrition: Joi.string().min(4).required(),
    total: Joi.number().required(),
  }).unknown(true);

  const schemaValidation = requiredSchema.validate(req.user);
  const schemaBoduValidation = requiredBodySchema.validate(req.body);
  
  if (schemaValidation.error) {
    return res.status(400).json({code: 400, data: { url: 'https://ciudadvirtual.co', message: 'Te falto información de usuario, la puedes llenar en tu usuario en este link.'}, error: schemaValidation.error});
  }
  if (schemaBoduValidation.error) {
    return res.status(400).json({code: 400, error: schemaBoduValidation.error, data: { url: 'https://ciudadvirtual.co', message: 'Te falto información de compra, manda la compra en un buen formato sino no genera compra..'}});
  }
  // 1. Get wenjoy url.
  const wenjoy = await wenjoyService.generatePaymentLink(req.user, req.body);
  logger.info('Wenjoy--SUCCESFULL');
  // 2. Create Purchase
  const purchase = await purchaseService.createPurchase(wenjoy, req.user, req.body);
  logger.info('Purchase--SUCCESFULL');
  // 3. Send Emails to Customer, Cree-ando & Brands.
  const to = req.user.email;
  const subject = 'Iniciaste una compra';
  const html = `
  <img src="https://cree-ando.com/wp-content/uploads/2021/04/Mundo-virtual-Cree-Ando-foro-2-1024x570.png" style="height: 100px; width: 100vw; object-fit: cover; border-radius: 25px 25px 0px 0px;">
  <h1>¡Has iniciado una compra!</h1>
  <p>
    Gracias ${req.user.name} por realizar 
    tu pedido en el marketplace de Cree-Ando.
  </p>
  <p>
    El identificador de tu compra es: ${wenjoy.purchase_id},
    en caso de que requieras compartir el pago con otra persona aqui esta el link de pago: ${wenjoy.payment_url}.
  </p>
  <img src="https://cree-ando.com/wp-content/uploads/2021/04/Mundo-virtual-Cree-Ando-foro-2-1024x570.png" style="height: 100px; width: 100vw; object-fit: cover; border-radius: 0px 0px 25px 25px;">
  `;
  /* const emailToCustomer = */ // await emailService.sendEmail(to, subject, html);
  // 4. Send Emails to Cree-ando & Brands.
  const toCreeAndo = 'vupisas@gmail.com';
  const subjectCreeAndo = 'Se realizo una compra';
  const htmlCreeAndo = `
  <img src="https://cree-ando.com/wp-content/uploads/2021/04/Mundo-virtual-Cree-Ando-foro-2-1024x570.png" style="height: 100px; width: 100vw; object-fit: cover; border-radius: 25px 25px 0px 0px;">
  <h1>¡Equipo: se inicio una compra!</h1>
  <p>
    El consumidor ${req.user.name} ha realizod una compra.
  </p>
  <p>
    El identificador de su compra es: ${wenjoy.purchase_id},
    El link de pago es: ${wenjoy.payment_url}.
  </p>
  <img src="https://cree-ando.com/wp-content/uploads/2021/04/Mundo-virtual-Cree-Ando-foro-2-1024x570.png" style="height: 100px; width: 100vw; object-fit: cover; border-radius: 0px 0px 25px 25px;">
  `;
  /* const emailtoCreeAndo = */ // await emailService.sendEmail(toCreeAndo, subjectCreeAndo, htmlCreeAndo);
  // 5. Send Emails to Cree-ando & Brands.
  const toBrand = 'vupisas@gmail.com';
  const subjectBrand = 'Se ha iniciado una orden';
  const htmlBrand = `
    <img src="https://cree-ando.com/wp-content/uploads/2021/04/Mundo-virtual-Cree-Ando-foro-2-1024x570.png" style="height: 100px; width: 100vw; object-fit: cover; border-radius: 25px 25px 0px 0px;">
    <h1>¡Equipo: se inicio una compra!</h1>
    <p>
      El consumidor ${req.user.name} ha realizod una compra, con tu marca. Recuerda que a penas haya pago por parte del consumidor se te habilitara la opción de processar y despachar la mercancia.
    </p>
    <p>
      En Cree-ando sabemos que si tu creces nosotros creceremos.
    </p>
    <img src="https://cree-ando.com/wp-content/uploads/2021/04/Mundo-virtual-Cree-Ando-foro-2-1024x570.png" style="height: 100px; width: 100vw; object-fit: cover; border-radius: 0px 0px 25px 25px;">
    `;
  /* const emailtoBrand =  */ // await emailService.sendEmail(toBrand, subjectBrand, htmlBrand);
  // 5. Return status with wenjoy payment url to client.
  res.status(httpStatus.CREATED).json({
    code: httpStatus.OK,
    message: 'Success: purchase created and wenjoy link provided',
    data: {
       ...purchase,
    },
  });
});

const getPurchasesById = catchAsync(async (req, res) => {
  const purchases = await purchaseService.getPurchasesById(req.user.id);
  res.status(httpStatus.OK).json({ code: httpStatus.OK, message: 'Success: All purchases', data: purchases });
});

module.exports = {
  createPurchase,
  getPurchasesById,
};
