const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const shippmentSchema = mongoose.Schema(
  {
    order: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Order',
    },
    payments: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Payment',
    },
    orderdBy: {
      type: mongoose.Schema.ObjectId,
      ref: 'User',
    },
    brand: {
      type: mongoose.Schema.ObjectId,
      ref: 'Brand',
    },
    visible: {
      type: Boolean,
      default: true,
    },
    transport: {
      type: String,
    },
    tracker: {
      type: Number,
    },
    cost: {
      type: Number,
    },
    recogida: {
      type: Object,
      default: {
        mensaje: ['Recogida con MU se ha generado'],
      },
    },
    rotulo: {
      type: String,
      default: '',
    },

  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
shippmentSchema.plugin(toJSON);
shippmentSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */

const Shippment = mongoose.model('Shippment', shippmentSchema);

module.exports = Shippment;
