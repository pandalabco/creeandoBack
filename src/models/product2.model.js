const moment = require('moment');
const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const product2Schema = mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
      maxlength: 62,
      text: true,
    },
    top: {
      type: Boolean,
      default: false,
    },
    visible: {
      type: Boolean,
      index: true,
      default: true,
    },
    slug: {
      type: String,
      unique: true,
      lowercase: true,
      index: true,
    },
    shortDesc: {
      type: String,
      required: true,
      maxlength: 2000,
      text: true,
    },
    price: {
      type: Number,
      required: true,
      trim: true,
      maxlength: 32,
    },
    discount:{
      type: Number,
      default: 0,
    },
    category: {
      type: mongoose.Schema.ObjectId,
      ref: 'Category',
      default: '607f8b0740e2c20ea7772dfd',
    },
    brand: {
      type: mongoose.Schema.ObjectId,
      ref: 'Brand',
    },
    subs: [
      {
        type: mongoose.Schema.ObjectId,
        ref: 'Sub',
        default: '607f8b7940e2c20ea7772e02',
      },
    ],
    variant: {
      type: [
        {
          color: {
            type: String,
            /* enum: [
              'N/A',
              'Negro',
              'Blanco',
              'Azul',
              'Verde',
              'Rojo',
              'Amarillo',
              'Vinotinto',
              'Morado',
              'Rosado',
              'Cafe',
              'Gris',
              'Naranja',
              'Celeste',
              'Magenta',
              'Otro',
            ], */
            default: 'N/A',
          },
          size: {
            type: String,
            /* enum: ['Única', '"XS"', 'S', 'M', 'L', 's', 'm', 'l', 'XL', '6', '8', '10', '12', '14'], */
            default: 'Única',
          },
          stock: {
            type: Number,
            required: true,
          },
          img: {
            type: String,
            required: true,
            default: 'https://cree-ando.com/wp-content/uploads/2021/04/Cree-Ando-logo-1.png',
          },
          visible: {
            type: Boolean,
            default: true,
          },
          price: {
            type: Number,
            trim: true,
            maxlength: 32,
            default: 0,
          },
          sku: {
            type: String,
            required: true,
            unique: true,
          },
        },
      ],
      required: true,
    },
    sold: {
      type: Number,
      default: 0,
    },
    pictures: {
      type: Array,
      required: true,
      default: ['https://cree-ando.com/wp-content/uploads/2021/04/Cree-Ando-logo-1.png'],
    },
    smPictures: {
      type: Array,
      required: true,
      default: ['https://cree-ando.com/wp-content/uploads/2021/04/Cree-Ando-logo-1.png'],
    },
    shipping: {
      type: String,
      enum: ['Si', 'No'],
      default: 'No',
    },
    ratings: {
      type: Number,
      default: 0,
    },
    reviews: {
      type: Number,
      default: 0,
    },
    created_at: {
      type: String,
      default: moment(),
    },
    updated_at_at: {
      type: String,
      default: moment(),
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
product2Schema.plugin(toJSON);
product2Schema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */

const Product = mongoose.model('Product2', product2Schema);

module.exports = Product;
